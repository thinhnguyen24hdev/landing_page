const headerNavList = document.querySelectorAll(".header-nav_item");
const headerNavIconMobile = document.querySelector(".header__navbar--icon");
const closeNavMobile = document.querySelector(".close-nav-mb");
const navItemMobile = document.querySelector(".header__nav--mobile");
const thirdItems = document.querySelectorAll(".account-item--mobile");
const navThirdItems = document.querySelectorAll(".nav-item__third-item");

headerNavList.forEach((item) => {
 const subNav = item.querySelector(".nav-item_list");

 item.addEventListener("mouseenter", () => {
  subNav.classList.add("slide-in");
  subNav.style.display = "block";
 });

 item.addEventListener("mouseleave", () => {
  subNav.classList.remove("slide-in");
  subNav.classList.add("slide-out");
  setTimeout(() => {
   subNav.style.display = "none";
   subNav.classList.remove("slide-out");
  }, 200);
 });
});

navThirdItems.forEach((item) => {
 const subNav = item.querySelector(".nav-account--pc");

 item.addEventListener("mouseenter", () => {
  subNav.classList.add("slide-in");
  subNav.style.display = "block";
 });

 item.addEventListener("mouseleave", () => {
  subNav.classList.remove("slide-in");
  subNav.classList.add("slide-out");
  setTimeout(() => {
   subNav.style.display = "none";
   subNav.classList.remove("slide-out");
  }, 200);
 });
});

//mobile
headerNavIconMobile.addEventListener("click", function () {
 navItemMobile.classList.add("active");
 //  navItemMobile.classList.add("nav-mobile-on");
 document.body.style.overflowY = "hidden";
});

closeNavMobile.addEventListener("click", function () {
 document.body.style.overflowY = "visible";
 //  navItemMobile.classList.remove("nav-mobile-on");
 //  navItemMobile.classList.add("nav-mobile-off");

 navItemMobile.classList.remove("active");
 //  setTimeout(() => {
 //   subNav.classList.remove("nav-mobile-off");
 //  }, 200);
});

function removeActiveClass() {
 navItemMobile.classList.remove("active");
 document.body.style.overflowY = "visible";
}

closeNavMobile.addEventListener("click", function () {
 removeActiveClass();
});

// Event listener for window resize
window.addEventListener("resize", function () {
 if (window.innerWidth >= 992) {
  removeActiveClass();
 }
});

thirdItems.forEach(function (thirdItem) {
 thirdItem.addEventListener("click", function () {
  const nextList = thirdItem.nextElementSibling;

  if (nextList && nextList.classList.contains("nav__acount--list")) {
   const isActive = nextList.classList.contains("active");

   if (isActive) {
    nextList.classList.remove("active");
   } else {
    document
     .querySelectorAll(".nav__acount--list.active")
     .forEach(function (activeList) {
      activeList.classList.remove("active");
     });
    nextList.classList.add("active");
   }
  }
 });
});

//count
const inputCheck = document.querySelector("#toggle");
const pricingAmount = document.querySelector(".pricing-left--des-number");

function countUp(upTo, num, countElement) {
 if (upTo <= num) {
  countElement.innerHTML = upTo;
  setTimeout(() => {
   countUp(++upTo, num, countElement);
  }, 20);
 }
}

function countDown(downTo, num, countElement) {
 if (downTo >= num) {
  countElement.innerHTML = downTo;
  setTimeout(() => {
   countDown(--downTo, num, countElement);
  }, 20);
 }
}

inputCheck.addEventListener("change", () => {
 if (inputCheck.checked) {
  countUp(pricingAmount.innerHTML, 49, pricingAmount);
 } else {
  countDown(49, 29, pricingAmount);
 }
});
