document.addEventListener("DOMContentLoaded", function () {
 const slidesContainer = document.querySelector(".intro__slider--left");
 const slides = document.querySelectorAll(".intro__slider--left-content");
 const imgs = document.querySelector(".intro__slider--img-item");

 const totalSlides = slides.length;
 const slideWidth = slides[0].clientWidth;
 let currentIndex = 0;
 let isDragging = false;
 let startPosX = 0;
 let currentTranslate = 0;
 let prevTranslate = 0;
 let isTransitioning = false;

 const firstClone = slides[0].cloneNode(true);
 const lastClone = slides[totalSlides - 1].cloneNode(true);

 slidesContainer.appendChild(firstClone);
 slidesContainer.insertBefore(lastClone, slidesContainer.firstElementChild);

 slidesContainer.style.transform = `translateX(${-slideWidth}px)`;

 const dataImages = [
  "./assets//images/photo-26.jpg",
  "./assets//images/photo-1.jpg",
 ];

 function slideToIndex(index) {
  imgs.classList.add("slide-right");
  setTimeout(() => {
   if (index > 0) imgs.setAttribute("src", dataImages[index - 1]);
   else imgs.setAttribute("src", dataImages[index + 1]);
  }, 100);

  setTimeout(() => {
   imgs.classList.remove("slide-right");
  }, 100);
  console.log(index);
  currentTranslate = -slideWidth * (index + 1);
  slidesContainer.style.transition = "transform 0.5s ease";
  slidesContainer.style.transform = `translateX(${currentTranslate}px)`;
 }

 function handleTransitionEnd() {
  if (currentIndex >= totalSlides) {
   currentIndex = 0;
   currentTranslate = -slideWidth;
  } else if (currentIndex < 0) {
   currentIndex = totalSlides - 1;
   currentTranslate = -slideWidth * totalSlides;
  }
  slidesContainer.style.transition = "none";
  slidesContainer.style.transform = `translateX(${currentTranslate}px)`;

  isTransitioning = false;
 }

 slidesContainer.addEventListener("transitionend", handleTransitionEnd);

 slidesContainer.addEventListener("mousedown", (e) => {
  isDragging = true;
  startPosX = e.clientX;
  prevTranslate = currentTranslate;
  slidesContainer.style.cursor = "grabbing";
 });

 slidesContainer.addEventListener("mousemove", (e) => {
  if (isDragging) {
   const currentPosition = e.pageX;
   currentTranslate = prevTranslate + currentPosition - startPosX;
   slidesContainer.style.transform = `translateX(${currentTranslate}px)`;
  }
 });

 slidesContainer.addEventListener("mouseup", () => {
  isDragging = false;
  slidesContainer.style.cursor = "grab";
  const movedBy = currentTranslate - prevTranslate;

  if (movedBy < -100 && currentIndex < totalSlides) {
   currentIndex++;
  } else if (movedBy > 100 && currentIndex > 0) {
   currentIndex--;
  }

  slideToIndex(currentIndex);
 });

 function nextSlide() {
  if (isDragging || isTransitioning) return;
  currentIndex++;
  slideToIndex(currentIndex);
  disableButtons();
 }

 function prevSlide() {
  if (isDragging || isTransitioning) return;
  currentIndex--;
  slideToIndex(currentIndex);
  disableButtons();
 }

 function disableButtons() {
  document.querySelector(".btn-next-slider").disabled = true;
  document.querySelector(".btn-prev-slider").disabled = true;
  setTimeout(() => {
   document.querySelector(".btn-next-slider").disabled = false;
   document.querySelector(".btn-prev-slider").disabled = false;
  }, 500);
 }

 document
  .querySelector(".btn-next-slider")
  .addEventListener("click", nextSlide);
 document
  .querySelector(".btn-prev-slider")
  .addEventListener("click", prevSlide);
});
