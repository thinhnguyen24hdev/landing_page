const form = document.getElementById("myForm");

form.addEventListener("submit", function (event) {
 event.preventDefault();
 validateForm();
});

const inputs = {
 cardName: "nameError",
 cardEmail: "emailError",
 cardPassword: "passwordError",
};

const errorMessages = {
 cardName: "Name is required",
 cardEmail: "Email is required",
 cardPassword: "Password must be at least 8 characters long",
};

for (const inputId in inputs) {
 const input = document.getElementById(inputId);
 const errorId = inputs[inputId];

 input.addEventListener("blur", function () {
  validateInput(inputId, errorId);
 });

 input.addEventListener("input", function () {
  clearError(errorId);
 });
}

function validateForm() {
 let isValid = true;

 for (const inputId in inputs) {
  const input = document.getElementById(inputId);
  const errorId = inputs[inputId];
  const errorElement = document.getElementById(errorId);
  const inputValue = input.value.trim();

  errorElement.textContent = "";

  if (!inputValue) {
   errorElement.textContent = errorMessages[inputId];
   isValid = false;
  } else if (inputId === "cardEmail" && !isValidEmail(inputValue)) {
   errorElement.textContent = "Invalid email address";
   isValid = false;
  } else if (inputId === "cardPassword" && inputValue.length < 8) {
   errorElement.textContent = errorMessages[inputId];
   isValid = false;
  }
 }

 if (isValid) {
  console.log("Name:", document.getElementById("cardName").value.trim());
  console.log("Email:", document.getElementById("cardEmail").value.trim());
  console.log(
   "Password:",
   document.getElementById("cardPassword").value.trim()
  );
 }
}

function validateInput(inputId, errorId) {
 const inputValue = document.getElementById(inputId).value.trim();
 const errorElement = document.getElementById(errorId);
 errorElement.textContent = "";

 if (!inputValue) {
  errorElement.textContent = errorMessages[inputId];
 } else if (inputId === "cardEmail" && !isValidEmail(inputValue)) {
  errorElement.textContent = "Invalid email address";
 } else if (inputId === "cardPassword" && inputValue.length < 8) {
  errorElement.textContent = errorMessages[inputId];
 }
}

function isValidEmail(email) {
 const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
 return emailRegex.test(email);
}

function clearError(errorId) {
 document.getElementById(errorId).textContent = "";
}
