const selectElement = document.getElementById("selected");
const searchInput = document.getElementById("search-input");
const showLimitItem = document.getElementById("show-limit-item");
const paginationContainer = document.getElementById("pagination-list");
const tableBody = document.getElementById("table-body");
const columns = document.querySelectorAll(".column_name");

let itemsPerPage = 10;
let currentPage = 1;
let textSearch = "";
let totalItems = data.length;
let totalPages = Math.ceil(totalItems / itemsPerPage);
let newDataTmp = [];
let sortByDes = undefined;
let columnName = undefined;

// create pagination
function generatePagination(totalPagesLimit) {
 paginationContainer.innerHTML = "";

 const maxVisiblePages = 5;
 let startPage = 1;
 let endPage = totalPagesLimit;

 if (totalPagesLimit > maxVisiblePages) {
  const halfVisiblePages = Math.floor(maxVisiblePages / 2);
  if (currentPage <= halfVisiblePages) {
   endPage = maxVisiblePages;
  } else if (currentPage >= totalPagesLimit - halfVisiblePages) {
   startPage = totalPagesLimit - maxVisiblePages + 1;
  } else {
   startPage = currentPage - halfVisiblePages;
   endPage = currentPage + halfVisiblePages;
  }
 }

 if (startPage > 1) {
  //   const firstPaginationItem = createPaginationItem(1);
  //   paginationContainer.appendChild(firstPaginationItem);
  const ellipsisItem = createEllipsis();
  paginationContainer.appendChild(ellipsisItem);
 }

 for (let i = startPage; i <= endPage; i++) {
  const paginationItem = createPaginationItem(i);
  paginationContainer.appendChild(paginationItem);
 }

 if (endPage < totalPagesLimit) {
  const ellipsisItem = createEllipsis();
  paginationContainer.appendChild(ellipsisItem);
  //   const lastPaginationItem = createPaginationItem(totalPages);
  //   paginationContainer.appendChild(lastPaginationItem);
 }

 const backStartButton = document.createElement("li");
 backStartButton.classList.add("pagination-item");
 backStartButton.innerHTML =
  '<i style="font-size: 11px" class="fa-solid fa-angle-double-left"></i>';
 if (currentPage === 1) {
  backStartButton.setAttribute("disabled", true);
  backStartButton.classList.add("disable");
 } else {
  backStartButton.addEventListener("click", function () {
   showPage(1, textSearch);
  });
 }

 const backButton = document.createElement("li");
 backButton.classList.add("pagination-item");
 backButton.innerHTML =
  '<i style="font-size: 11px" class="fa-solid fa-angle-left"></i>';
 if (currentPage === 1) {
  backButton.setAttribute("disabled", true);
  backButton.classList.add("disable");
 } else {
  backButton.addEventListener("click", function () {
   showPage(currentPage - 1, textSearch);
  });
 }

 paginationContainer.insertBefore(backButton, paginationContainer.firstChild);
 paginationContainer.insertBefore(
  backStartButton,
  paginationContainer.firstChild
 );

 const nextButton = document.createElement("li");
 nextButton.classList.add("pagination-item");
 nextButton.innerHTML =
  '<i style="font-size: 11px" class="fa-solid fa-chevron-right"></i>';
 if (currentPage === totalPages || totalPages === 0) {
  nextButton.setAttribute("disabled", true);
  nextButton.classList.add("disable");
 } else {
  nextButton.addEventListener("click", function () {
   showPage(currentPage + 1, textSearch);
  });
 }
 paginationContainer.appendChild(nextButton);

 const nextEndButton = document.createElement("li");
 nextEndButton.classList.add("pagination-item");
 nextEndButton.innerHTML =
  '<i style="font-size: 11px" class="fa-solid fa-angles-right"></i>';
 if (currentPage === totalPages || totalPages === 0) {
  nextEndButton.setAttribute("disabled", true);
  nextEndButton.classList.add("disable");
 } else {
  nextEndButton.addEventListener("click", function () {
   showPage(totalPages, textSearch);
  });
 }
 paginationContainer.appendChild(nextEndButton);
}

function createPaginationItem(page, textSearch) {
 const paginationItem = document.createElement("li");
 paginationItem.classList.add("pagination-item");
 paginationItem.textContent = page;

 if (page === currentPage) {
  paginationItem.classList.add("active");
 }

 paginationItem.addEventListener("click", function () {
  showPage(page, textSearch);
 });

 return paginationItem;
}

// create ...
function createEllipsis() {
 const ellipsisItem = document.createElement("li");
 ellipsisItem.classList.add("pagination-item");
 ellipsisItem.textContent = "...";
 return ellipsisItem;
}

function sortDataByColumn(dataSort, columnName, isDescending) {
 return dataSort.sort((a, b) => {
  const valueA = a[columnName];
  const valueB = b[columnName];

  let comparison = 0;
  if (valueA > valueB) {
   comparison = 1;
  } else if (valueA < valueB) {
   comparison = -1;
  }

  if (isDescending) {
   comparison *= -1;
  }

  return comparison;
 });
}

function showPage(page, textSearch) {
 currentPage = page;
 generatePagination(totalPages);
 displayData(page, textSearch);
}

//show data
function displayData(page, textSearch) {
 textSearch = textSearch === undefined ? "" : textSearch;
 let newData = data.filter((item) =>
  item.Name.toLowerCase().includes(textSearch.toLowerCase())
 );
 totalItems = newData.length;
 const startIndex = (page - 1) * itemsPerPage;
 const endIndex = Math.min(startIndex + itemsPerPage, totalItems);

 if (sortByDes !== undefined) {
  newData = sortDataByColumn(newData, columnName, sortByDes);
 }

 showLimitItem.innerHTML = `Showing ${
  newData.length === 0 ? "0" : startIndex + 1
 } to ${endIndex} of ${
  data.length === newData.length
   ? `${data.length} entries`
   : `${newData.length} entries (filtered from ${data.length} total entries)`
 }  `;

 tableBody.innerHTML = "";

 for (let i = startIndex; i < endIndex; i++) {
  const row = document.createElement("tr");

  const columnsToShow = [
   "Name",
   "Position",
   "Office",
   "Age",
   "Start date",
   "Salary",
  ];
  columnsToShow.forEach((column) => {
   const cell = document.createElement("td");
   cell.textContent = newData[i][column];
   row.appendChild(cell);
  });

  tableBody.appendChild(row);
 }
}

//change limit page
selectElement.addEventListener("change", function () {
 itemsPerPage = Number(selectElement.value);
 totalPages = Math.ceil(totalItems / itemsPerPage);
 newDataTmp = data.filter((item) =>
  item.Name.toLowerCase().includes(textSearch.toLowerCase())
 );
 totalPages = Math.ceil(newDataTmp.length / itemsPerPage);
 generatePagination(totalItems);
 showPage(1, textSearch);
});

//search
searchInput.addEventListener("input", function () {
 textSearch = searchInput.value === undefined ? "" : searchInput.value.trim();
 newDataTmp = data.filter((item) =>
  item.Name.toLowerCase().includes(textSearch.toLowerCase())
 );
 totalPages = Math.ceil(newDataTmp.length / itemsPerPage);
 generatePagination(totalPages);
 showPage(1, textSearch);
});

//sort
columns.forEach((column) => {
 column.addEventListener("click", function () {
  const columnNameTmp = column.innerHTML;
  const isDescending = column.classList.contains("sort_desc");
  columns.forEach((col) => col.classList.remove("sort_desc"));
  columns.forEach((col) => col.classList.remove("sort_asc"));

  if (!isDescending) {
   column.classList.add("sort_desc");
   sortByDes = true;
   columnName = columnNameTmp.split("<div")[0].trim().replaceAll("/n", "");
  } else {
   column.classList.add("sort_asc");
   sortByDes = false;
  }

  showPage(currentPage, textSearch);
 });
});

//Start
generatePagination(totalPages);
showPage(currentPage, textSearch);
